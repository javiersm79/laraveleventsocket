<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventName implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $data;
    public $canal;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($canal,$dato)
    {
        $this->data = $dato;
        $this->canal = $canal;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
         return ['canal'.$this->canal];
    }
    public function broadcastWith()
    {
      return [
        'nombre' => "javiersm79",
        'datoServer' => $this->data
        ];
    }
}
