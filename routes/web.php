<?php

use App\Events\EventName;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/{canal}', function ($canal) {
    return view('welcome', ['canal' => $canal]);
});
Route::get('/fire/{canal}/{dato}', function ($canal,$dato) {
    Event::fire(new EventName("$canal","$dato"));
    return "canal: $canal / dato: $dato";
});
