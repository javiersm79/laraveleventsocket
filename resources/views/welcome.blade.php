<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>
            Laravel
            @if (isset($dato))
                        {{$dato}}
                        
                    @endif

        </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel<br>
                    @if (isset($canal))
                        {{$canal}}
                        <input type="text" id="canal" value="{{$canal}}">
                    @endif

                </div>

                
 

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
<script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
<script>
var socketURL = 'http://localhost:3001'; // 192.168.10.10 can be replaced with the IP address of your server.
canal='canal'+document.getElementById('canal').value
 //alert(canal)
// If you are using Elixir/Browserify use commented out socket instead.
var socket = io(socketURL);
// var socket = require('socket.io-client')(socketURL);

// The event name is created by the event channel (example-event) we set earlier
// and the class name with the full namespace (App\Events\EventName).
//socket.on('event-example:App\\Events\\EventName', function (event) {
socket.on(canal+':App\\Events\\EventName', function (event) {
    console.log(event);
});

</script>
    </body>
</html>
